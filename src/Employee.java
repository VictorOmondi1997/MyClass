/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author VictorOmondi
 */

import java.util.Scanner;
public class Employee {
    private String name;
    private short yob;
    private int idNo;
    private double grossSalary;
    private static Scanner reader=new Scanner(System.in);
    
    public Employee(){
        System.out.println("Creating a new employee");
        System.out.println("Enter Name");
        name=reader.nextLine();
        System.out.println("Enter id Number");
        idNo=reader.nextInt();
        System.out.println("Enter Year of Birth");
        yob=reader.nextShort();
        System.out.println("Enter Gross Salary For " +name);
        grossSalary=reader.nextDouble();
    }
    public void getTax(){
        double net,
               tax;
        tax=0.3*grossSalary;
        net=grossSalary-tax;
        System.out.println("Your Tax="+tax+"Your Net Salary ="+ net);
        System.out.println("Kama ni kidogo Shauri yako");
    }
    public double getSalary(){
        return grossSalary;
    }
    public void setSalary(){
        double amt;
        System.out.println("Your Current Salary is "+grossSalary);
        System.out.println("Enter Amount To Change Salary");
        amt=reader.nextDouble();
        grossSalary=grossSalary+amt;
        System.out.println("Your  New Salary ="+grossSalary);
        System.out.println("Hio iko fiti au bado ni kidogo? "+name);
    }
    public void setName(String n){
        System.out.println("Your Current Name: "+name);
        System.out.println("Not Your Name?\nEnter new name");
        n=reader.nextLine();
        n=name;
    }
    public String getName(){
        return name;
    }
    public void getDetails(){
        System.out.println("Name: "+name);
        System.out.println("ID Number: "+idNo);
        System.out.println("Salary: "+grossSalary);
        System.out.println("Year of Birth: "+yob);
    }
    public static void main(String[] args){
        Employee emp1 = new Employee();
        String yourName= "Vict";
        //emp1.getSalary();
        //emp1.getTax();
        emp1.setName(yourName);
        emp1.getName();
    }
}
